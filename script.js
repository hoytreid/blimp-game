// Global Constants
const gameArea = document.getElementById("game-area");
const context = gameArea.getContext("2d");
const centerX = gameArea.width / 2;
const centerY = gameArea.height / 2;

// Global Variables
let frame = 0;
let obstacles = [];
let boundaries = [[],[]];
let gameSpeed = -3;
let pauseGeneration = false;
let gameStart = false;
let gameOver = false;
let showText = true;

// Game Objects
let player = {
  email: "",
  marketing: false,
  score: 0,
  height: 90,
  width: 171,
  positionX: 96,
  positionY: centerY,
  gravity: 0.05,
  gravitySpeed: 0,
  colour: "#333",
  render: function() {
    let sprite = new Image();
    sprite.src = "img/vector-blimp.svg";
    context.drawImage(sprite, this.positionX, this.positionY, this.width, this.height);
  },
  updatePosition: function() {
    this.gravitySpeed += this.gravity;
    if (this.positionY < 0) {
      this.gravitySpeed = 0;
      this.positionY += 1;
    }
    this.positionY += this.gravitySpeed;
  },
  updateScore: function() {
    if (currentInterval(5)) {
      this.score++;
    }
    renderOverlay(0, 0, gameArea.width, 60);

    context.font = "bold 30px Calibri";
    context.textAlign = "left";
    context.fillStyle = "#fff";
    context.fillText("Score: " + this.score, 30, 40);
  }
}

function Obstacle(height, width, image, position, speed) {
  this.height = height;
  this.width = width;
  this.positionY = position;
  this.positionX = gameArea.width;
  this.speed = speed;
  this.render = function() {
    let sprite = new Image();
    sprite.src = image;
    context.drawImage(sprite, this.positionX, this.positionY, this.width, this.height);
  };
  this.collisionDetection = function() {
    if (player.positionX + (player.width - 5) > this.positionX + 10 &&
        player.positionX < this.positionX + (this.width / 2) &&
        player.positionY + (player.height - 5) > this.positionY + 10 &&
        player.positionY < this.positionY + 10 + (this.height - 20)) {
      gameOver = true;
    }
  }
}

let obstacleHandler = {
  obstacleList: [
    {
      height: 200,
      width: 290,
      image: "img/vector-cloud1.svg"
    },
    {
      height: 267,
      width: 248,
      image: "img/vector-cloud2.svg"
    },
    {
      height: 271,
      width: 340,
      image: "img/vector-cloud3.svg"
    }
  ],
  generate: function(interval) {
    if (currentInterval(interval) && !pauseGeneration) {
      const randomObstacle = this.obstacleList[Math.floor(Math.random() * this.obstacleList.length)];
      const maxPosition = gameArea.height - randomObstacle.height;
      const minPosition = 0;
      let position = Math.floor(Math.random() * (maxPosition - minPosition + 1) + minPosition);
      obstacles.push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, position, gameSpeed));
    }
  },
  update: function() {
    for (let i = 0; i < boundaries[0].length; i++) {
      if (boundaries[0][i].positionX < -300) {
        boundaries[0].splice(i, 1);
        boundaries[1].splice(i, 1);
      }
      boundaries[0][i].positionX += boundaries[0][i].speed;
      boundaries[0][i].render();
      boundaries[0][i].collisionDetection();

      boundaries[1][i].positionX += boundaries[1][i].speed;
      boundaries[1][i].render();
      boundaries[1][i].collisionDetection();
    }

    for (let i = 0; i < obstacles.length; i++) {
      if (obstacles[i].positionX < -350) {
        obstacles.shift();
      }
      if (obstacles.length !== 0) {
      obstacles[i].positionX += obstacles[i].speed;
      obstacles[i].render();
      obstacles[i].collisionDetection();
      }
    }
  },
  framing: function(interval) {
    const randomObstacle = this.obstacleList[Math.floor(Math.random() * this.obstacleList.length)];
    if (currentInterval(interval)) {
      boundaries[0].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, 0 - (randomObstacle.height - 50), -3));
      boundaries[1].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, gameArea.height - 75, -3));
    }
  },
  initialFraming: function(startNumber, interval) {
    const randomObstacle = this.obstacleList[Math.floor(Math.random() * this.obstacleList.length)];
    for (let i = 0; i < startNumber; i++) {
      const randomObstacle = this.obstacleList[Math.floor(Math.random() * this.obstacleList.length)];
      boundaries[0].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, 0 - (randomObstacle.height - 50), -3));
      boundaries[1].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, gameArea.height - 75, -3));
      for(let j = 0; j < boundaries[0].length; j++) {
        boundaries[0][j].positionX += boundaries[0][j].speed * interval;
        boundaries[1][j].positionX += boundaries[1][j].speed * interval;
      }
    }
    boundaries[0].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, 0 - (randomObstacle.height - 50), -3));
    boundaries[1].push(new Obstacle(randomObstacle.height, randomObstacle.width, randomObstacle.image, gameArea.height - 75, -3));
  }
}

// Game Screens
function startScreen() {
  obstacleHandler.generate(550);
  for (let i = 0; i < obstacles.length; i++) {
    obstacles[i].positionX += -1;
    obstacles[i].render();
  }

  renderOverlay(0, 0, gameArea.width, gameArea.height);

  let sprite = new Image();
  sprite.src = "img/vector-blimp.svg";
  context.drawImage(sprite, 221, 60, 581, 307);

  renderText("30px Calibri", 4, "Lift the blimp by holding space or", centerX, centerY + 65);
  renderText("30px Calibri", 4, "touching the screen", centerX, centerY + 105);
  renderText("30px Calibri", 4, "Avoid the clouds and get a high score!", centerX, centerY + 145);

  if (showText == true) {
    renderText("900 60px Exo", 8, "Press space or", centerX, gameArea.height - 155);
    renderText("900 60px Exo", 8, "touch to start!", centerX, gameArea.height - 85);
  }
}

function gameOverScreen() {
  renderOverlay(0, 0, gameArea.width, gameArea.height);

  renderText("900 60px Exo", 8, "Game over!", centerX, 210);
  renderText("36px Calibri", 6, "Your final score was:", centerX, centerY - 110);
  renderText("bold 60px Calibri", 6, player.score, centerX, centerY - 40);
  renderText("24px Calibri", 4, "Enter your E-mail below to submit your score", centerX, centerY + 40);

  scoreSubmissionForm.render();

  clearInterval(gameRun);
}

let scoreSubmissionForm = {
  positionY: (gameArea.clientHeight / 2) + (gameArea.clientHeight / 100) * 9,
  positionX: (gameArea.clientWidth / 2) - (gameArea.clientWidth / 100) * 33,
  inputWidth: (gameArea.clientWidth / 100) * 66,
  inputHeight: (gameArea.clientHeight / 100) * 7.5,
  restartWidth: (gameArea.clientWidth / 100) * 31.5,
  submitWidth: (gameArea.clientWidth / 100) * 29.5,
  buttonHeight: (gameArea.clientHeight / 100) * 9.5,
  submitForm: document.getElementById('score-submit'),
  inputField: document.getElementById('player-email'),
  submitButton: document.getElementById('submit-button'),
  restartButton: document.getElementById('restart-button'),
  render: function() {
    this.inputField.style.width = `${this.inputWidth}px`;
    this.inputField.style.height = `${this.inputHeight}px`;

    if (gameArea.clientWidth < 786) {
      this.inputField.style.borderRadius = "10px";
      this.inputField.style.borderWidth = "2px";
      this.inputField.style.fontSize = "18px";
    }

    if (gameArea.clientWidth < 550) {
      this.inputField.style.borderRadius = "5px";
    }

    if (gameArea.clientWidth < 450) {
      this.inputField.style.borderWidth = "1px";
      this.inputField.style.fontSize = "14px";
    }

    this.restartButton.style.width = `${this.restartWidth}px`;
    this.restartButton.style.height = `${this.buttonHeight}px`;
    this.submitButton.style.width = `${this.submitWidth}px`;
    this.submitButton.style.height = `${this.buttonHeight}px`;

    this.submitForm.style.top = `${this.positionY}px`;
    this.submitForm.style.left = `${this.positionX}px`;
    this.submitForm.style.display = "block";
  },
  hide() {
    this.submitForm.style.display = "none";
  },
  submitHandler: function(e) {
    //score submission code
    e.preventDefault();
    marketingScreen();
  },
  restartHandler: function() {
    document.location.reload();
  }
}

function marketingScreen() {
  context.clearRect(0, 0, gameArea.width, gameArea.height);
  for (let i = 0; i < obstacles.length; i++) {
    obstacles[i].render();
  }
  for (let i = 0; i < boundaries[0].length; i++) {
    boundaries[0][i].render();
    boundaries[1][i].render();
  }
  player.render();
  renderOverlay(0, 0, gameArea.width, gameArea.height);

  scoreSubmissionForm.hide();
  marketingForm.render();

  renderText("900 60px Exo", 8, "Enjoy this game?", centerX, 210);
  renderText("36px Calibri", 6, "It was made by a trainee of 3 months!", centerX, centerY - 110);
  renderText("30px Calibri", 6, "Get involved in our immersive", centerX, centerY - 28);
  renderText("30px Calibri", 6, "developer training programme", centerX, centerY + 8);
  renderText("30px Calibri", 6, "as a sponsor, partner or trainee", centerX, centerY + 44);
  renderText("30px Calibri", 6, "with one click.", centerX, centerY + 80);

  renderText("italic 18px Calibri", 4, "We will use your supplied E-mail address to send you", centerX, gameArea.height - 110);
  renderText("italic 18px Calibri", 4, "an information pack about our Scion Coalition Scheme.", centerX, gameArea.height - 88);
  renderText("italic 18px Calibri", 4, "We never sell to third parties.", centerX, gameArea.height - 66);
}

let marketingForm = {
  positionY: (gameArea.clientHeight / 2) + (gameArea.clientHeight / 100) * 21,
  positionX: (gameArea.clientWidth / 2) - (gameArea.clientWidth / 100) * 27,
  actionWidth: (gameArea.clientWidth / 100) * 53.5,
  buttonHeight: (gameArea.clientHeight / 100) * 9.5,
  marketingForm: document.getElementById('marketing-submit'),
  actionButton: document.getElementById('action-button'),
  render: function() {
    this.actionButton.style.width = `${this.actionWidth}px`;
    this.actionButton.style.height = `${this.buttonHeight}px`;

    this.marketingForm.style.top = `${this.positionY}px`;
    this.marketingForm.style.left = `${this.positionX}px`;
    this.marketingForm.style.display = "block";
  },
  actionHandler: function() {
    //marketting submission code
    player.marketing = true;
  },
}

// Utility functions
function currentInterval(n) {
  if ((frame / n) % 1 == 0) {
    return true;
  } else {
    return false;
  };
}

function renderText(fontStyle, lineWidth, text, posX, posY) {
  context.font = fontStyle;
  context.textAlign = "center";
  context.strokeStyle =  "#2a2a3a";
  context.lineWidth = lineWidth;
  context.lineJoin = "round";
  context.strokeText(text, posX, posY);
  context.fillStyle = "#fff";
  context.fillText(text, posX, posY);
}

function renderOverlay(startX, startY, width, height) {
  context.beginPath();
  context.rect(startX, startY, width, height);
  context.fillStyle = "rgba(16, 16, 36, 0.25)";
  context.fill();
}

// Event Handlers
function keyDownHandler(e) {
  if(e.keyCode == 32 && gameStart == true) {
    player.gravity = -0.15;
  } else if (e.keyCode == 32 && gameStart == false) {
    gameStart = true;
    frame = 0;
    obstacles = [];
    clearInterval(textBlink);
  }
}

function keyUpHandler(e) {
  if(e.keyCode == 32 && gameStart == true) {
    player.gravity = 0.05;
  }
}

function touchStartHandler(e) {
  if(gameStart == true) {
    player.gravity = -0.15;
  } else if (gameStart == false) {
    gameStart = true;
    obstacles = [];
  }
}

function touchEndHandler() {
  player.gravity = 0.05;
}

// Event Listeners
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("touchstart", touchStartHandler, false);
document.addEventListener("touchend", touchEndHandler, false);
scoreSubmissionForm.submitButton.addEventListener("click", scoreSubmissionForm.submitHandler, false);
scoreSubmissionForm.restartButton.addEventListener("click", scoreSubmissionForm.restartHandler, false);

// Run game
const gameRun = setInterval(update, 10);
const textBlink = setInterval(() => showText = !showText, 1000);
obstacleHandler.initialFraming(5, 150);

function update() {
  frame++;
  context.clearRect(0, 0, gameArea.width, gameArea.height);

  if (gameOver == true) {
    obstacleHandler.update();
    player.updatePosition();
    player.render();
    gameOverScreen();
  } else if (gameStart == true) {
    if(currentInterval(2500) && gameSpeed > -10) {
      gameSpeed -= 1;
      pauseGeneration = true;
      setTimeout(() => pauseGeneration = false, 1000);
    }
    obstacleHandler.generate(345 + gameSpeed * 15);
    obstacleHandler.framing(150);
    obstacleHandler.update();
    player.updatePosition();
    player.render();
    player.updateScore();

    if (player.positionY + player.gravity > gameArea.height - (player.height / 2)) {
      gameOver = true;
    }
  } else {
    startScreen();
  }
}
